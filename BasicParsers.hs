{-# LANGUAGE TypeFamilies #-}
module BasicParsers where

import Data.Void
import Control.Monad (void)
import Text.Megaparsec
import Text.Megaparsec.Char as C
import qualified Text.Megaparsec.Char.Lexer as L


type Parser = Parsec Void String

sc :: Parser ()
sc = let lineCmnt  = L.skipLineComment "--"
         blockCmnt = L.skipBlockComment "{-" "-}" 
     in L.space space1 lineCmnt blockCmnt


lexeme :: Parser a -> Parser a
lexeme = L.lexeme sc


anyChar :: Parser Char
anyChar = lexeme C.anyChar

integer :: Parser Int
integer = lexeme L.decimal


double :: Parser Double
double =  lexeme $ L.signed sc L.float


symbol :: String -> Parser String
symbol = L.symbol sc