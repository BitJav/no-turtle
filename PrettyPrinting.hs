module PrettyPrinting (showCompComm) where

import TypesAndConstants

showCompComm :: CompCom -> String
showCompComm cc = showCC cc 0  

showCC :: CompCom -> Int -> String
showCC (Rep n cc)        m = addIndent m "rep " ++ show n ++ "\n" ++ showCC cc (m+1)
showCC (Cmd c)           m = addIndent m "" ++ showCommand c
showCC (CCSeq xs)        m = addIndent m "[\n" ++ showCCList xs (m+1) ++ "\n" ++ addIndent m "]\n"
showCC (IncRep n xs)     m = addIndent m "incompleteRep " ++ show n ++ " [\n" ++ showCCList xs (m+1)
showCC (IncCompCom cc)   m = addIndent m (showCC cc m) ++ ";"
showCC (FinalCompCom cc) m = addIndent m (showCC cc m) ++ ")"


showCCList :: [CompCom] -> Int -> String
showCCList []       n = ""
showCCList [cc]     n = showCC cc n
showCCList (cc:ccs) n = showCC cc n ++ ",\n" ++ showCCList ccs n


showCommand :: Command -> String
showCommand (Fw n)     = "fw " ++ show n
showCommand (Rot d)    = "rot " ++ show d
showCommand (Chg c)    = "chg " ++ show c
showCommand Clear      = "clear"
showCommand Reset      = "reset"



addIndent :: Int -> String -> String
addIndent 0 s = "" ++ s
addIndent n s = "\t" ++ addIndent (n-1) s