all:
	ghc --make Main.hs -threaded

clean:
	rm -rf *.hi; rm -rf *.o;
	rm Main
