# Introducción

Este es un intérprete de una versión reducida del lenguaje de programación [*Logo*](https://es.wikipedia.org/wiki/Logo_(lenguaje_de_programaci%C3%B3n)).

A continuación se listan los comandos del programa:

## Comandos gráficos:

  * *fw **n***: Avanza una cantidad ***n*** de pixeles en el ángulo actual
  * *rot **d***: Rota una cantidad igual a ***d*** el ángulo, en el sentido de las agujas del reloj. Esto afecta a futuros trazos
  * *chg **c***: Cambia a ***c*** el color de futuros trazos
  * *rep **n** ( **cmd** )*: Repite ***n*** veces el comando ***cmd***. Los paréntesis son necesarios
  * *clear*: Borra el dibujo actual
  * *reset*:
    * Borra el dibujo actual
    * Setea el centro del área de dibujo como el origen del trazo  
    * Setea el ángulo a 0º
    * Setea el color a negro
  
#### Secuencias de comandos:

  Se pueden escribir secuencias de comandos separando cada uno con **;**

  Por ejemplo


    fw 100; rot 30; chg verde; fw 50

#### Comando en varias líneas:

  Se puede escribir un comando en varias líneas como se muestra a continuación:

    Ejemplo 1:
      fw 30; rot 10; fw 50;
      rot -90; chg rojo; fw 100

    Ejemplo 2:
      rep 5 (rot 45; fw 60;
      rot 45)

## Comandos de control:

  * *:save **filePath***: Guarda el dibujo actual en el archivo ***filePath*** con formato png
  * *:load **filePath***: Dibuja los comandos que toma del archivo ***filePath***
  * *:help*: Muestra la ayuda
  * *:exit*: Sale del programa
  
## Instalación

Los comandos que se muestran a continuación corresponden a un una distribución Ubuntu 16.04.
En caso de utilizar otras distribuciones los mismos deberán ser modificados levemente.

#### Dependencias

Previa instalación del programa se necesitan las siguientes dependencias de terceros
    
* libgtk2.0-dev
* libpango1.0-dev
* libcairo2-dev
* libghc-x11-dev
                      
las cuales pueden instalarse con el comando
  ```
  $ sudo apt-get install libgtk2.0-dev libpango1.0-dev libcairo2-dev libghc-x11-dev
  ```

Se recomienda instalar [stack](https://docs.haskellstack.org/en/stable/README/) para evitar inconvenientes
a la hora de resolver dependencias con bibliotecas de Haskell.

También deben ser instaladas las herramientas de las que depende la biblioteca gtk2hs.
Ésto puede hacerse usando [stack](https://docs.haskellstack.org/en/stable/README/):
  ```
  $ stack install gtk2hs-buildtools
  ```

#### Programa
Habiendo instalado las dependencias se debe ejecutar el siguiente comando
```
$ stack setup; stack build
```


## Iniciar programa

El programa se inicia ejecutando 
```
$ stack exec noTurtle
```

## Código de ejemplo

Ver carpeta de ejemplos